<?php
// +----------------------------------------------------------------------

return [
	'default' => [
		// 服务器地址
		'hostname' => '127.0.0.1',
		// 数据库名
		'database' => 'admin',
		// 用户名
		'username' => 'admin',
		// 密码
		'password' => 'fDE4285ThHTHaH2c',
		// 端口
		'hostport' => '3306',
		// 连接dsn
		'dsn' => '',
		// 数据库连接参数
		'params' => [],
		// 数据库编码默认采用utf8
		'charset' => 'utf8mb4',
		// 数据库表前缀
		'prefix' => '',
	],
	'spare' => [
		'type' => 'mysql',
		'hostname' => '127.0.0.1',
		'database' => 'admin',
		'username' => 'admin',
		'password' => 'fDE4285ThHTHaH2c',
		'hostport' => '3306',
      'charset' => 'utf8mb4',
	],
];
